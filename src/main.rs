use gtk::prelude::*;
use gtk::{Application, ApplicationWindow};

fn main() {
    let app = Application::builder()
        .application_id("org.gtk-rs.example")
        .build();

    app.connect_activate(build_ui);

    app.run();

}

fn build_ui( app: &Application) {
    let window = ApplicationWindow::builder()
        .application(app)
        .title("Lets go")
        .build();

    window.present();
}

